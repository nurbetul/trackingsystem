<?php

Route::get('/', 'HomeController@login')->name('login');
Route::get('/kullanici/giris', 'LoginController@get_login')->name('user-login');
Route::post('/kullanici-giris/post', 'LoginController@post_form')->name('login-post');
Route::get('/kayit-ol', 'RegisterController@getRegister')->name('get-register');
Route::post('/yeni-kullanici', 'RegisterController@postRegister')->name('post-register');
Route::get('/aktiflestir/{slug}', 'RegisterController@getActivation')->name('get-activation');
Route::get('/cikis', 'LoginController@getLogout')->name('get-logout');


Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'admin'], function () {
        Route::get('/kullanici', 'AdminController@getUser')->name('get-user');
        Route::get('/yeni-kullanici', 'UserController@getUserForm')->name('get-user-new');
        Route::get('/kullanici-guncelle/{id}', 'UserController@getUserForm')->name('get-user-arrangement');
        Route::post('/kullanici-kaydet/{id}', 'UserController@postUserCreated')->name('post-user-created');
        Route::get('/kullanici-sil/{id}', 'UserController@getDelete')->name('get-delete');
        Route::get('/gorev', 'AdminController@getTask')->name('get-task');
        Route::get('/yeni-gorev', 'TaskController@getTaskForm')->name('get-task-new');
        Route::get('/gorev-guncelle/{id}', 'TaskController@getTaskForm')->name('get-task-arrangement');
        Route::post('/gorev-kaydet/{id}', 'TaskController@postTaskCreated')->name('post-task-created');
        Route::get('/görev-sil/{id}', 'TaskController@getDeleteTask')->name('get-delete-task');

    });
});
Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'projeyoneticisi'], function () {
        Route::get('/', 'SupervisorController@getSupervisor')->name('get-supervisor-login');
        Route::get('/gorevler', 'SupervisorController@getTaskSupervisor')->name('get-task-supervisor');
        Route::get('/yeni-gorev','TaskController@getSupervisorTaskForm')->name('get-supervisor-task-new');
        Route::get('/gorev-guncelle/{id}','TaskController@getSupervisorTaskForm')->name('get-supervisor-task-arrangement');
        Route::post('/gorev-kaydet/{id}','TaskController@postSupervisorTaskCreated')->name('post-supervisor-task-created');
        Route::get('gorev/kontrol/{id}/{description}','SupervisorController@getSupervisorDescriptionChange')->name('get-supervisor-description-change');
        Route::post('/yildizlar/{id}', 'SupervisorController@postPerformance')->name('post-performance');
        Route::get('/yildiz/{id}', 'SupervisorController@getPerformance')->name('get-performance');
    });

});

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'kullanici'], function () {
        Route::get('/', 'UserController@getUser')->name('get-user-login');
        Route::get('/mesaj', 'UserController@getMessage')->name('get-message');
        Route::get('/yeni-mesaj', 'UserController@getNewMessage')->name('get-new-message');
        Route::post('/yeni-mesajlar', 'UserController@postNewMessage')->name('post-new-message');
        Route::get('/mesaj-sil/{id}', 'UserController@getDeleteMessage')->name('get-delete-message');
        Route::get('/islem/{id}/{description}', 'UserController@getDescriptionChange')->name('get-description-change');
    });

});




Auth::routes();







